/* Created by Alexander Nuikin (nukisman@gmail.com) on 26.01.17. */
'use strict';

const lib = require('@shared/lib');
lib.then(console.log); // prints: 'lib promise'

const nargiz = require('nargiz');
console.log({nargiz});