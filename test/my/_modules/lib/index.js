/* Created by Alexander Nuikin (nukisman@gmail.com) on 26.01.17. */
'use strict';

const _ = require('@shared/lodash');
console.log('_: squares:', _.map([1, 2, 3], x => x * x));

const when = require('@shared/when');
module.exports = when.resolve('lib promise');