### Features

* Local modules followed by symbolic links (no need reinstall it each time)
    * `node --preserve-symlinks` follows symlinks 
    * It's ok if you `npm publish` your project which code contains `require('@shared/...')`
    * Symlinks are **relative** so you can save it in repository (better than `npm link` with absolute symlinks)
    * Zip follows symlinks so your app.zip contains all it need (no broken links)
* Local modules are shared between each other
* **No dependencies duplication** for shared local modules
* Single version of shared wrapper module (like `lodash` or else)
    * Also **w/o duplication**
* Recursive commands for installing and cleaning regular and shared modules
  
### Change log
  
TODO  

### Install

`npm install --global shmod`

### Usage

```bash
> shmod
Usage:
  shmod find      - Show app's shared dependencies
  shmod link      - Create links for app's shared dependencies in node_modules/@shared/
  shmod clean     - Run recursive 'npm prune' for app and it's local modules (appSharedDependencies.localModules)
  shmod install   - Run recursive 'npm install' for app and it's local modules (appSharedDependencies.localModules)
  shmod reinstall - Shmod clean and install
  shmod build     - Shmod install and link
  shmod rebuild   - Shmod clean an build
  shmod zip path/to/app.zip  - Zip app with correct symlink following
Version: ...
```

### Example

TODO: Fix example:

* 1 local lib
* 2 apps used shared:
    * local lib
    * 3rd-party npm-modules
        * regular/legacy
        * dependent on some @shared/x

Initial directories layout:

```
my/
|--_modules/
|  |- versions.json     # {dependencies: {when: "3.7.7", xxx: "1.2.3", joi: "10.2.0"}}
|  \- lib/              # Shared library module
|     |- index.js
|     \- package.json   # sharedDependencies: ["when", "lodash"]
\--apps/
   |- _modules/         # Shared libs alsho could be there   
   |  \- versions.json  # {dependencies: {lodash: "4.17.4"}}
   |- app1/
   |  |- index.js
   |  \- package.json   # appSharedDependencies: {nodeModules: ["when"], localModules.parent: ["lib"]}
   \- app2/
      |- node_modules/
      |  |- xxx/
      |  \- package.json # sharedDependencies: ["joi"]
      |- index.js
      \- package.json   # appSharedDependencies: {nodeModules: ["xxx"], localModules.custom: {lib: "../../_modules/lib"}}
```

#### Shared library module

Write your custom library in `my/_modules/lib/index.js`:
```js
const _ = require('@shared/lodash');
console.log('_: squares:', _.map([1, 2, 3], x => x * x));

const when = require('@shared/when');
module.exports = when.resolve('lib promise');
```
Specify shared dependencies for the app in `my/_modules/lib/package.json` used directly in the lib:
```json
{
  "sharedDependencies": [
    "when",
    "lodash"
  ]
}
```

#### Module installed from the npm-registry

`my/apps/app2/node_modules/xxx/index.js` specifies uses dependencies:
```js
const Joi = require('@shared/joi');
module.exposrt = {isJoi: Joi.string().isJoi}
```
`my/apps/app2/node_modules/xxx/package.json` specifies shared dependencies:
```json
{
  "sharedDependencies": [
    "joi"
  ]
}
```

#### App 1 

Write some code in `my/apps/app1/index.js`:
```js
const lib = require('@shared/lib');
lib.then(console.log); // prints: 'lib promise'

const when = require('@shared/when');
when.resolve('app promise')
  .then(console.log)
``` 
Specify shared dependencies for the app in `my/apps/app1/package.json` used directly in the app:
```json
{
  // Specify modules directly in the app code, not in libraries
  "appSharedDependencies": {
    // Links refers to node_modules/* - zipped as symlinks
    "nodeModules": [
      "when"
    ],
    // Links refeers als to node_modules/* but zipped as folders
    "localModules": {
      // Find up to the tree ../*/_modules/lib
      "parent": [
        "lib"
      ]
    }
  }
}
```

#### App 2 

Write some code in `my/apps/app2/index.js`:
```js
const lib = require('@shared/lib');
lib.then(console.log); // prints: 'lib promise'

const xxx = require('@shared/xxx');
console.log({xxx});

const yyy = require('yyy');
console.log({yyy});
``` 
Specify shared dependencies for the app in `my/apps/app1/package.json` used directly in the app:
```json
{
  // Specify modules directly in the app code, not in libraries
  "appSharedDependencies": {
    // Links refers to node_modules/* - zipped as symlinks
    "nodeModules": [ // Installed via npm, required as '@shared/xxx'
      "xxx"
    ],
    // Links refeers also to node_modules/* but zipped as folders
    "localModules": {
      // Any relative/absolute path
      "custom": {
        "lib": "../../_modules/lib"
      }
    }
  },
  "dependencies": { // Installed via npm, required as 'yyy'
    "yyy": "1.2.3"
  }
}
```

Install shared local dependencies from upper `../*/_modules/' directories:
```bash
> cd a/b/app
> shmod build
cd /Users/urukhai/Job/nu/npm/shmod/test/a/_modules/when && npm install
cd /Users/urukhai/Job/nu/npm/shmod/test/a/b/_modules/lib && npm install
cd /Users/urukhai/Job/nu/npm/shmod/test/a/_modules/lodash && npm install
cd /Users/urukhai/Job/nu/npm/shmod/test/a/b/_modules/joi && npm install
cd /Users/urukhai/Job/nu/npm/shmod/test/a/b/app && npm install
Link: node_modules/@shared/when -> ../../../../_modules/when
Link: node_modules/@shared/lib -> ../../../_modules/lib
Link: node_modules/@shared/lodash -> ../../../../_modules/lodash
Link: node_modules/@shared/joi -> ../../../_modules/joi
```

#### Run app

```bash
> cd a/b/app
> node --preserve-symlinks index.js     # --preserve-symlinks required for working with node_modules/@shared/* symlinks 
_: squares: [ 1, 4, 9 ]
{ isJoi: true }
lib promise
app promise
```

#### Results

* Check `a/b/app/node_modules/@shared/*` - there are local (**relative**) only links! Not absolute like `npm link`!
* All your links to shared local modules in your git repository!   
* Check `a/b/app/node_modules`: there is **no duplication** for `when` while it used from `lib` and `app`!
* Version of `when` specified once only in `a/_modules/when/package.json`!
* Version of `lodash` specified once only in `a/_modules/lodash/package.json`!
* Version of `joi` specified once only in `a/b/_modules/joi/package.json`!
* All shared and regular dependencies of the app was installed via single run `shmod build`!

### Git ignore

Ignore anything in `node_modules` but not `@shared` 

```ignore
**/node_modules/*
!**/node_modules/@shared
```

## Feedback

Feel free to add issue: https://bitbucket.org/nukisman/shmod/issues