
### Version 3.0.x

* `shmod <zip | link | find | (re)install | clean | (re)build>`

* `shmod zip ...` - custom zip options

* NOT NEED ANYMORE: `shmod wrap ()`

* `shmod (re)install|build`  - Find `_modules/versions.json` in parent folders for override version of module X from `package.json`:`dependenies` using `npm i --save X`
* `shmod (re)install|build [path/to/versions.json]` - Specify custom versions file
* `versions.json` format: `{dependencies: {a: "1.2.3"}, extend?: "path/to/parent/version.json"}`

* Option: `shmod (re)install|build [-v]` - Don't use `version.json` in parent folders, just run `npm i` to use existed `package.json`:`dependenies`

* `shmod (re)install|build`  - Runs also `npm install DEP` for `sharedDependencies` of libs installed from npm (`node_modules/**`)

* `Option: -q | --quite` for minimalistic output (show errors only)