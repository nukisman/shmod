/* Created by Alexander Nuikin (nukisman@gmail.com) on 31.01.17. */
'use strict';

const childProcess = require('mz/child_process');

const print = ([stdout, stderr]) => {
  if (stdout) console.log(stdout);
  if (stderr) console.error(stderr);
};

module.exports = (dir, cmd) => {
  const command = `cd ${dir} && ${cmd}`;
  console.log(command);
  return childProcess.exec(command)
    .then(print);
};