/* Created by Alexander Nuikin (nukisman@gmail.com) on 26.01.17. */
'use strict';
const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp');
const _ = require('lodash');

const MODULES_DIR_NAME = '_modules';
const VERSION_FILE_NAME = 'versions.json';

const findShared = (name, dir) => {
  const pathItems = dir.split(path.sep);
  let found;
  for (let i = 1; i < pathItems.length; i++) {
    const modules = path.join(pathItems.slice(0, -i).join('/'), MODULES_DIR_NAME, name);
    try {
      const stat = fs.statSync(modules);
      if (stat.isDirectory()) {
        found = {stat, path: modules};
        break;
      }
    } catch (error) {
      if (error.code !== 'ENOENT') {
        throw error;
      }
    }
  }
  return found
}

const findRecursive = (dir, list, rootSharedDeps) => {
  // console.log('Dir:', dir);
  if (!dir.startsWith('/')) dir = path.join(process.cwd(), dir);

  const sharedDeps = rootSharedDeps || require(path.join(dir, 'package.json')).sharedDependencies;

  if (sharedDeps) {
    // console.log('Shared dependencies:', sharedDeps.join(' '));
    sharedDeps.forEach(name => {
      const found = findShared(name, dir);
      if (found) {
        // console.log('Found:', found.path);
        list.push({name: name, path: found.path});
        findRecursive(found.path, list);
      }
      else list.push({name: name, path: undefined})
    })
  }
};

const findLibShared = (dir, rootSharedDeps) => {
  const list = [];
  findRecursive(dir, list, rootSharedDeps);
  const map = {}
  _.uniq(list).forEach(f => {
    if (f.path) map[f.name] = f.path
    else map[f.name] = null
  });
  return map;
};

const findNpmShared = (appDir, pkgDir, npmSharedDeps) => {
  // console.log({pkgDir});
  const package_json = require(path.join(pkgDir, 'package.json'));
  const sharedDeps = package_json.sharedDependencies;
  // console.log('> ', pkgDir, sharedDeps)
  if (sharedDeps && _.isArray(sharedDeps)) {
    npmSharedDeps.push(...sharedDeps);
  }

  const node_modules = path.join(pkgDir, 'node_modules');
  let files;
  try {
    files = fs.readdirSync(node_modules);
  } catch (error) {
    if (error.code === 'ENOENT') {
      //console.log(`WARN: ${path.relative(appDir, node_modules)} not found`);
      // do nothing
    }
    else throw error;
  }
  if(files) {
    files.forEach(file => findNpmShared(appDir, require(path.join(pkgDir, file), npmSharedDeps)));
  }
};

const findVersions = (dir) => {
  const versionsPath = path.join(dir, MODULES_DIR_NAME, VERSION_FILE_NAME);
  // console.log({versionsPath});
  let versions_json;
  try {
    versions_json = require(versionsPath);
  } catch (error) {
    if (error.code === 'MODULE_NOT_FOUND') {
      // do nothing
    }
    else throw error
  }
  const parent = path.dirname(dir);
  if (parent === '/') return {};
  else return _.merge({}, findVersions(parent), versions_json || {});
};

const modulesVersioned = (modules, versions) => modules.reduce((acc, name) => {
  const v = versions.dependencies ? versions.dependencies[name] : null;
  if (v) acc.push(`${name}@${v}`);
  else acc.push(name);
  return acc
}, [])

function find(appDir) {
  const appShDeps = require(path.join(appDir, 'package.json')).appSharedDependencies;
  if (!appShDeps) {
    console.log('WARN: appSharedDependencies not specified in package.json');
    return {}
  }
  else {
    // console.log(appShDeps)
    const nodeModules = appShDeps.nodeModules || [];
    if (appShDeps.localModules) {

      /*******************************************************************************************
       * Local modules: ../.../_modules/ location
       *******************************************************************************************/
      let parentLocalLibs = {}, parentNpmLibs = []
      if (appShDeps.localModules.parent) {
        const parentDeps = findLibShared(appDir, appShDeps.localModules.parent);
        parentLocalLibs = _.reduce(parentDeps, (acc, libPath, name) => {
          if (libPath !== null) acc[name] = libPath
          return acc
        }, {})
        parentNpmLibs = _.reduce(parentDeps, (acc, libPath, name) => {
          if (libPath === null) acc.push(name)
          return acc
        }, [])
      }
      // console.log('Parent npm libs:', parentNpmLibs);
      // console.log('Parent local libs:', parentLocalLibs);

      /*******************************************************************************************
       * Local modules: custom location
       *******************************************************************************************/
      const customLocalLibs = {};
      _.each(appShDeps.localModules.custom || {}, (libPath, name) => {
        try {
          const p = path.isAbsolute(libPath) ? libPath : path.join(appDir, libPath);
          fs.statSync(p)
          customLocalLibs[name] = path.resolve(appDir, libPath);
        } catch (error) {
          if (error.code === 'ENOENT')
            console.log(`WARN: Local module '${name}' not found: ${libPath}`);
          else throw error
        }
      })
      // console.log('Custom local libs:', customLocalLibs);

      /*******************************************************************************************
       * Shared dependencies of 3rd-party npm modules
       *******************************************************************************************/
      const node_modules = path.join(appDir, 'node_modules');
      let files;
      try {
        files = fs.readdirSync(node_modules);
      } catch (error) {
        if (error.code === 'ENOENT') {
          console.log(`WARN: ${path.relative(appDir, node_modules)} not found`);
        }
        else throw error;
      }
      const npmSharedDeps = [];
      if (files) {
        files.forEach(file => {
          if (file !== '@shared' && !file.startsWith('.')) {
            const pkgDir = path.join(appDir, 'node_modules', file);
            findNpmShared(appDir, pkgDir, npmSharedDeps);
          }
        });
      }
      // console.log('3rd-party shared modules:', npmSharedDeps);

      /*******************************************************************************************
       * Local modules: merge and check conflicts
       *******************************************************************************************/
      const localLibs = _.merge(parentLocalLibs, customLocalLibs);
      if (localLibs.length < parentLocalLibs.length || localLibs.length < customLocalLibs.length) {
        const localLibsConflicts = _.intersection(_.keys(parentLocalLibs), _.keys(customLocalLibs));
        localLibsConflicts.forEach(name => {
          console.log(`WARN: Local lib ${name} conflict:`);
          console.log("WARN:   parent:", parentLocalLibs[name]);
          console.log("WARN:   custom:", customLocalLibs[name]);
        })
      }
      // console.log('Local libs:', localLibs);

      const nodeMods = _.uniq([...nodeModules, ...parentNpmLibs, ...npmSharedDeps]);
      return {
        nodeModules: nodeMods,
        nodeModulesVersioned: modulesVersioned(nodeMods, findVersions(appDir)),
        localModules: localLibs
      }
    }
    else return {
      nodeModules,
      nodeModulesVersioned: modulesVersioned(nodeModules, findVersions(appDir)),
      localModules: {}
    }
  }
}

module.exports = find;