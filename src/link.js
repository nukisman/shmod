/* Created by Alexander Nuikin (nukisman@gmail.com) on 25.01.17. */
'use strict'
const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp');
const _ = require('lodash');

module.exports = (dir, shared) => {
  const sharedDir = `${dir}/node_modules/@shared`;
  mkdirp.sync(sharedDir);
  fs.readdirSync(sharedDir).forEach(file => fs.unlinkSync(`${sharedDir}/${file}`)); // Remove existed links

  const makeLink = (t, name) => {
    const link = `${sharedDir}/${name}`;
    const target = path.relative(sharedDir, t);
    console.log('Link:', path.relative(dir, link), '->', target);
    try {
      fs.symlinkSync(target, link);
    } catch (error) {
      if (error.code === 'EEXIST') {
        const stat = fs.lstatSync(link);
        if (stat.isSymbolicLink()) {
          fs.unlinkSync(link);
          fs.symlinkSync(target, link);
        }
        else console.log(link, 'already exists and it is not a symbolic link')
      }
      else throw error
    }
  };

  _.forEach(shared.localModules, makeLink);

  _.forEach(shared.nodeModules, name => {
    const t = path.join(dir, 'node_modules', name);
    makeLink(t, name);
  });
}