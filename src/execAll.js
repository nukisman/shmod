/* Created by Alexander Nuikin (nukisman@gmail.com) on 31.01.17. */
'use strict';

const when = require('when');
const _ = require('lodash');
const exec = require('./exec');

module.exports = (dirs, cmd) => when.all(dirs.map(dir => exec(dir, cmd)));