/* Created by Alexander Nuikin (nukisman@gmail.com) on 26.01.17. */
'use strict';

const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp');

module.exports = (name, version) => {
  const PATH = process.cwd();
// console.log({version})


  const content = {
    'package.json': JSON.stringify({
      "description": "Shared module",
      "main": "index.js",
      "license": "ISC",
      "repository": "unknown",
      "dependencies": {
        [name]: version
      }
    }, null, 2),
    'index.js': `'use strict';\nmodule.exports = require('${name}');`
  }

  const paths = {
    'package.json': path.join(PATH, 'package.json'),
    'index.js': path.join(PATH, 'index.js')
  };

  // Assert files not exists
  for(let i in paths) {
    const p = paths[i];
    try {
      fs.statSync(p);
      console.log(`${i}:\tFile already exists`);
    } catch (error) {
      if (error.code === 'ENOENT') {
        console.log(`${i}:\tCreate`)
        fs.writeFileSync(p, content[i], 'utf8');
      }
      else throw error
    }
  }
}