#!/usr/bin/env node

/* Created by Alexander Nuikin (nukisman@gmail.com) on 26.01.17. */
'use strict';
const _ = require('lodash');
const when = require('when');
const path = require('path');

const find = require('./find');
const exec = require('./exec');
const execAll = require('./execAll');
const link = require('./link');

const cmd = process.argv[2];

const dir = process.cwd();

const usage = `
Usage:
  shmod find      - Show app's shared dependencies
  shmod link      - Create links for app's shared dependencies in node_modules/@shared/
  shmod clean     - Run recursive 'npm prune' for app and it's local modules (appSharedDependencies.localModules)
  shmod install   - Run recursive 'npm install' for app and it's local modules (appSharedDependencies.localModules)
  shmod reinstall - Shmod clean and install
  shmod build     - Shmod install and link
  shmod rebuild   - Shmod clean an build
  shmod zip path/to/app.zip  - Zip app with correct symlink following 
Version: ${require('../package.json').version}
`;

class Shmod {
  constructor(dir) {
    this.dir = dir;
    this.shared = undefined;
  }

  print(title) {
    const char = '#'
    const n = 30;
    console.log(`${char.repeat(n)}\n${char} ${title}\n${char.repeat(n)}`)
  }

  find() {
    if (!this.shared) {
      this.print('Find');
      // _.forEach(find(dir), (p, name) => console.log(`${name}:\t${p}`));
      const found = find(dir);
      console.log('Shared node modules: ' + found.nodeModulesVersioned.join(' '));
      console.log('Shared local modules:\n' + _.map(found.localModules, (p, name) => `  ${name}: ${p}`).join('\n'));
      this.shared = found;
    }
  }

  link() {
    this.find(dir);
    this.print('Link');
    return when.promise((resolve, reject) => {
      resolve(link(dir, this.shared));
    });
  }

  clean() {
    this.find(dir);
    this.print('Clean');
    const dirs = [..._.values(this.shared.localModules), dir];
    return execAll(dirs, 'npm prune')
      .then(() => exec(dir, `rm -fR ${path.join(dir, 'node_modules/@shared')}`));
  }

  install() {
    this.find(dir);
    this.print('Install');
    /*
     * package.json:dependencies
     */
    const dirs = [..._.values(this.shared.localModules), dir];
    return execAll(dirs, 'npm install')
      .then(() => {
        /*
         * nodeModules
         */
        return exec(dir, `npm install -SE ` + this.shared.nodeModulesVersioned.join(' '));
      });
  }

  reinstall() {
    return this.clean()
      .then(() => this.install());
  }

  build() {
    return this.install()
      .then(() => this.link());
  }

  rebuild() {
    return this.clean()
      .then(() => this.build());
  }

  zip(target) {
    this.find(dir);
    this.print('Zip');
    const local = _.keys(this.shared.localModules).map(k => `./node_modules/\@shared/${k}`);
    const options = '-9rq';
    return exec(dir, `zip ${options} --symlinks ${target} ./ ${local.map(p => `-x ${p}`).join(' ')}`)
      .then(() => exec(dir, `zip ${options} ${target} ${local.join(' ')}`));
  }
}

const shmod = new Shmod(dir);

switch (cmd) {
  case 'find':
  case 'link':
  case 'clean':
  case 'install':
  case 'reinstall':
  case 'build':
  case 'rebuild':
    shmod[cmd]();
    break;
  case 'zip':
    const targetZip = process.argv[3];
    if(targetZip) {
      shmod.zip(targetZip);
    }
    else console.log(usage);
    break;
  default:
    console.log(usage);
}